import 'package:flutter/material.dart';
import 'package:flutter_projects/bottom_bar_item.dart';

class BottomBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 70,
        child: ColoredBox(
          color: Color(0xFFFAFAFC),
          child: DecoratedBox(
            decoration: BoxDecoration(
              border: Border(
                top: BorderSide(
                  color: Color(0xFFDEDBDE),
                ),
              ),
            ),
            child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    BottomBarItem(
                      icon: Icons.search,
                      color: Color(0xFF979497),
                      text: 'Search',
                    ),
                    BottomBarItem(
                      icon: Icons.videogame_asset_rounded,
                      color: Color(0xFF979497),
                      text: 'Arcade',
                    ),
                    BottomBarItem(
                      icon: Icons.apps,
                      color: Color(0xFF979497),
                      text: 'Apps',
                    ),
                    BottomBarItem(
                      icon: Icons.gamepad,
                      color: Color(0xFF979497),
                      text: 'Games',
                    ),
                    BottomBarItem(
                      icon: Icons.today,
                      color: Color(0xFF017BFF),
                      text: 'Today',
                    ),
                  ],
                ),
              ),
              Spacer(),
            ],
          ),
        ),
      ),
    );
  }
}
