import 'package:flutter/material.dart';
import 'package:flutter_projects/item.dart';
import 'package:flutter_projects/second_page.dart';

class ListViewItem extends StatefulWidget {
  @override
  _ListViewItemState createState() => _ListViewItemState();
}

class _ListViewItemState extends State<ListViewItem> with SingleTickerProviderStateMixin{
  AnimationController anim;

  @override
  void initState() {
    super.initState();

    anim = AnimationController(vsync: this, duration: Duration(milliseconds: 500), value: 0.0);
  }
  @override
  void dispose() {
    anim.dispose();
    //print('dispose');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () =>
          Navigator.of(context).push(
            PageRouteBuilder(
              transitionDuration: Duration(milliseconds: 500),
              pageBuilder: (ctx, anim1, anim2) {
                return SecondPage(tag: this.hashCode, anim: anim1, anim2: anim);
              },
            ),
          ),
      child: Hero(
        flightShuttleBuilder: (ctx1, anim,_, ctx2, ctx3) {
          return Item(anim: anim);
        },
        tag: this.hashCode,
        child: Item(),
      ),
    );
  }
}
