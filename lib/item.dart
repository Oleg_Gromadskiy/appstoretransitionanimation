import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_projects/const.dart';
import 'package:flutter_projects/cyberpunk.dart';

class Item extends StatefulWidget {
  final Animation<double> anim;
  Animation<double> anim2;

  Item({Key key, this.anim}) : super(key: key);

  @override
  _ItemState createState() => _ItemState();
}

class _ItemState extends State<Item> {
  ScrollController _controller;

  @override
  void initState() {
    super.initState();

    _controller = ScrollController();
    _controller.addListener(() => setState(() => print(_controller.offset)));


    widget.anim2 = Tween<double>(begin: 0.5, end: 1.0).animate(widget.anim);
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        SingleChildScrollView(
          controller: _controller,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              widget.anim == null ? _buildImage(context) : _buildAnimatedImage(context),
              widget.anim == null ? _buildText(context) : _buildAnimatedText(context),
            ],
          ),
        ),
        Positioned(
          bottom: getPosition(),
          left: 10,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(20),
            child: Material(
              color: Colors.transparent,
              child: ColoredBox(
                color: Color(0xFFF7F4F7),
                child: SizedBox(
                  width: MediaQuery.of(context).size.width - 20,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        DecoratedBox(
                          decoration: BoxDecoration(
                            color: Color(0xFFE6E6E8),
                            borderRadius: BorderRadius.circular(25),
                          ),
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: 12,
                              vertical: 4,
                            ),
                            child: Text(
                              '\$4.99',
                              style: TextStyle(color: Colors.blue),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Column(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Text(
                                    'Cyberpunk 2077 mobile',
                                    style: TextStyle(
                                      color: Colors.black87,
                                    ),
                                  ),
                                  SizedBox(height: 5),
                                  Text(
                                    'action',
                                    style: TextStyle(
                                      color: Colors.black87,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(width: 10),
                              Image.network(
                                URL2,
                                width: 48,
                                height: 48,
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        Positioned(
          left: 10,
          top: 10,
          child: Material(
            color: Colors.transparent,
            child: IconButton(
              onPressed: () {
                SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
                Navigator.of(context).pop();
              },
              icon: Icon(
                Icons.cancel,
                color: Colors.white.withOpacity(0.7),
                size: 32 * (widget.anim == null ? .0 : widget.anim.value),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildImage(BuildContext context) {
    return Flexible(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(20),
          child: Cyberpunk(),
        ),
      ),
    );
  }

  Widget _buildAnimatedImage(BuildContext context) {
    return AnimatedBuilder(
      animation: widget.anim,
      builder: (ctx, child) => Flexible(
        child: Padding(
          padding: EdgeInsets.only(
            bottom: .0,
            top: 10.0 - (10.0 * widget.anim.value),
            left: 10.0 - (10.0 * widget.anim.value),
            right: 10.0 - (10.0 * widget.anim.value),
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20.0 - (20.0 * widget.anim.value)),
              topRight: Radius.circular(20.0 - (20.0 * widget.anim.value)),
              bottomRight: widget.anim.value < 0.5 ? Radius.circular(20.0 - (20.0 * widget.anim.value)) : Radius.zero,
              bottomLeft: widget.anim.value < 0.5 ? Radius.circular(20.0 - (20.0 * widget.anim.value)) : Radius.zero,
            ),
            child: ColoredBox(
              color: Colors.white,
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20.0 - (20.0 * widget.anim.value)),
                  topRight: Radius.circular(20.0 - (20.0 * widget.anim.value)),
                  bottomRight: Radius.circular(20.0 - (20.0 * widget.anim.value)),
                  bottomLeft: Radius.circular(20.0 - (20.0 * widget.anim.value)),
                ),
                child: Cyberpunk(),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildText(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 0,
      child: ColoredBox(
        color: Colors.white,
        child: Padding(
          padding: EdgeInsets.all(10),
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Text(lorem),
          ),
        ),
      ),
    );
  }

  Widget _buildAnimatedText(BuildContext context) {
    return AnimatedBuilder(
      animation: widget.anim,
      builder: (ctx, child) => Padding(
        padding: EdgeInsets.only(
          bottom: 10.0 - (10.0 * widget.anim.value),
          top: .0,
          left: 10.0 - (10.0 * widget.anim.value),
          right: 10.0 - (10.0 * widget.anim.value),
        ),
        child: SizedBox(
          width: double.infinity,
          height: widget.anim.value != 1.0 ? MediaQuery.of(context).size.height * 0.4 * convert(widget.anim.value) : null,
          child: ColoredBox(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
              child: Material(
                color: Colors.transparent,
                child: Text(lorem),
              ),
            ),
          ),
        ),
      ),
    );
  }

  double convert(double value) {
    if (value < 0.5)
      return 0;
    else
      return (value - 0.5) * 2;
  }

  double getPosition() {
    if (_controller.positions.isEmpty) {
      return -1000;
    }

    if (_controller.offset - MediaQuery.of(context).size.height * 0.6 >= 25)
      return 25;
    else
      return _controller.offset - MediaQuery.of(context).size.height * 0.6;
  }
}
