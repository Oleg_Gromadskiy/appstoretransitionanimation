import 'package:flutter/material.dart';

class BottomBarItem extends StatelessWidget {
  final IconData icon;
  final Color color;
  final String text;

  const BottomBarItem({Key key, this.icon, this.color, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Icon(
          icon,
          size: 36,
          color: color,
        ),
        Text(text),
      ],
    );
  }
}
