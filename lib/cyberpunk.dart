import 'package:flutter/material.dart';

import 'const.dart';

class Cyberpunk extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.6,
      width: double.infinity,
      child: Material(
        color: Colors.transparent,
        child: Stack(
          children: [
            Image.network(
              URL,
              fit: BoxFit.cover,
              height: double.infinity,
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: Padding(
                padding: EdgeInsets.only(
                  left: 20,
                  bottom: 10,
                  right: 20,
                ),
                child: Row(
                  children: [
                    SizedBox(
                      child: DecoratedBox(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(25),
                        ),
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 12,
                            vertical: 4,
                          ),
                          child: Text(
                            '\$4.99',
                            style: TextStyle(color: Colors.blue),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Text(
                                'Cyberpunk 2077 mobile',
                                style: TextStyle(
                                  color: Colors.white,
                                  shadows: [
                                    Shadow(
                                      color: Colors.black45,
                                      offset: Offset(1, 1),
                                    ),
                                    Shadow(
                                      color: Colors.black45,
                                      offset: Offset(0, 1),
                                    ),
                                    Shadow(
                                      color: Colors.black45,
                                      offset: Offset(1, 0),
                                    ),
                                    Shadow(
                                      color: Colors.black45,
                                      offset: Offset(0, 0),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(height: 5),
                              Text(
                                'action',
                                style: TextStyle(
                                  color: Colors.white,
                                  shadows: [
                                    Shadow(
                                      color: Colors.black45,
                                      offset: Offset(1, 1),
                                    ),
                                    Shadow(
                                      color: Colors.black45,
                                      offset: Offset(0, 1),
                                    ),
                                    Shadow(
                                      color: Colors.black45,
                                      offset: Offset(1, 0),
                                    ),
                                    Shadow(
                                      color: Colors.black45,
                                      offset: Offset(0, 0),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          Image.network(
                            URL2,
                            width: 64,
                            height: 64,
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            Positioned(
              right: 10,
              bottom: 150,
              child: Text(
                'lag\nof the\nyear\nedition'.toUpperCase(),
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 32,
                  shadows: [
                    BoxShadow(
                      color: Colors.black45,
                      offset: Offset(-1, -1),
                    ),
                    BoxShadow(
                      color: Colors.black45,
                      offset: Offset(1, -1),
                    ),
                    BoxShadow(
                      color: Colors.black45,
                      offset: Offset(-1, 1),
                    ),
                    BoxShadow(
                      color: Colors.black45,
                      offset: Offset(1, 1),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
