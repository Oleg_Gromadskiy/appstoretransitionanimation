import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_projects/bottom_bar.dart';
import 'package:flutter_projects/list_view_item.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(statusBarColor: Color(0xFFEEEBEE).withOpacity(0.6), statusBarIconBrightness: Brightness.dark),
    );
    return MaterialApp(
      title: 'Material App',
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Color(0xFFEEEBEE),
        body: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.only(
                top: 10.0,
                left: 10.0,
                right: 10.0,
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ClipOval(
                    child: Image.network(
                      'https://image.made-in-china.com/2f0j00JOgTZAYCkeuc/Cute-Dog-Clothes-Pokemon-Winter-Pet-Products.jpg',
                      height: 38,
                      width: 38,
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        'THURSDAY, JULY 16',
                        style: TextStyle(
                          color: Color(0xFF7B787B),
                        ),
                      ),
                      Text(
                        'Today',
                        style: TextStyle(
                          fontSize: 36,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            ListViewItem(),
            ListViewItem(),
            ListViewItem(),
          ],
        ),
        bottomNavigationBar: BottomBar(),
      ),
    );
  }
}
