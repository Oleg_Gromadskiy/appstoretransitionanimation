import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_projects/item.dart';

class SecondPage extends StatelessWidget {
  final int tag;
  final Animation<double> anim;
  final AnimationController anim2;

  const SecondPage({Key key, this.tag, this.anim, this.anim2}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    return NotificationListener<OverscrollNotification>(
      onNotification: (n){
        print(n);
        return true;
      },
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Hero(
          tag: tag,
          child: Item(
            anim: anim,
          ),
        ),
      ),
    );
  }
}
